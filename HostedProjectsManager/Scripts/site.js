﻿
$(function() {

    GetDirectoryList();

    function GetDirectoryList() {
        ShowLoader();

        $.ajax({
            type: "get",
            url: "/Home/DirectoryDetails",
            async: false,
            success: function(data) {
                DisplayDirectoryList(data);
                HideLoader();
            },
            error: function() {
                alert("error");
                HideLoader();
            }
        });
    }

    function CloseLoginModal() {
        $('#changeFtpSuccess').hide();
        $('#changeFtpError').hide();
        $('#loginContainer').hide();
    }

    function OpenLoginModal() {
        $('#loginContainer').show();
    }

    InitLoginFieldsWatermark();

    function InitLoginFieldsWatermark() {

        $('#host').watermark('Host name', { className: 'watermark' });
        $('#login').watermark('Login', { className: 'watermark' });
        $('#password').watermark('Password', { className: 'watermark' });
    }

    $('#hostName').click(function() {

        if ($('#loginContainer').is(':visible') == true) {
            CloseLoginModal();
        }
        else {
            OpenLoginModal();
        }
    });

    $('#ChangeFtp').click(function() {

        ShowLoader();

        $.ajax({
            type: "get",
            url: "/Home/ChangeFtp",
            async: false,
            data: { domain: $('#host').val(), login: $('#login').val(), password: $('#password').val() },
            success: function(data) {

                if (data.IsError == false) {
                    GetDirectoryList();
                    HideEditor();

                    $('#changeFtpSuccess').show();
                    $('#changeFtpError').hide();

                    $('#hostName').text($('#host').val());

                    $('#loginContainer').delay('2000').fadeOut(900,
                                                                       function() {
                                                                           $('#changeFtpSuccess').hide();
                                                                       });

                }
                else {
                    $('#changeFtpSuccess').hide();
                    $('#changeFtpError').show();
                }

                HideLoader();
            },
            error: function() {
                alert("error");
                HideLoader();
            }
        });
    });

    $("#refresh").click(function() {
        GetDirectoryList();
    });

    $('#toggleDirectoryPanel').click(function() {

        if ($(this).attr('status') == 'visible') {
            $('#leftPanel').width('1px');
            $('#rightPanel').css({ 'left': '1px' });
            $(this).attr('status', 'hidden');
        }
        else {
            $('#leftPanel').width('299px');
            $('#rightPanel').css({ 'left': '300px' });
            $(this).attr('status', 'visible');
        }

    });

    $('#directoryContainer').delegate('.fileItem', 'click', FileItemClick);

    var currentFile;

    function FileItemClick(e) {

        ShowLoader();

        var $this = $(this);

        var fileName = $this.attr('fileName');
        var folderName = $this.parents('.folderItem').attr('folderName');

        LoadFile(fileName, folderName);

        //need to reset
        currentFile = { name: fileName, path: folderName };

        e.stopPropagation();
    }

    function LoadFile(fileName, folderName) {
        $.ajax({
            type: "get",
            url: "/Home/ReadFile",
            async: false,
            data: { file: fileName, directory: folderName },
            success: function(data) {

                $('#fileContent').attr('currentFile', fileName);

                $('#fileContent').attr('currentDirectory', folderName);

                $('#fileContent').val(data.Content);

                var ext = fileName.replace(/.*\.(.*)/, '$1');

                var language = 'HTML';

                switch (ext) {
                    case 'aspx':
                        language = 'asp';
                        break;
                    case 'ascx':
                        language = 'asp';
                        break;
                    case 'cs':
                        language = 'csharp';
                        break;
                    case 'css':
                        language = 'CSS';
                        break;
                    case 'js':
                        language = 'JavaScript';
                        break;
                }

                $('#fileName').text(fileName);

                if (folderName == '' || folderName == undefined) {
                    $('#filePath').text('root');
                }
                else {
                    $('#filePath').text(folderName + '/');
                }

                fileContentEditor.edit('fileContent', language);
                HideLoader();

                ShowEditor();
            },
            error: function() {
                HideLoader();
                alert("error");
            }
        });
    }

    $('#SaveFileContent').click(function() {

        ShowLoader();

        $.ajax({
            type: "post",
            url: "/Home/SaveFile",
            async: false,
            data: { file: $('#fileContent').attr('currentFile'), content: fileContentEditor.getCode(), directory: $('#fileContent').attr('currentDirectory') },
            success: function(data) {
                HideLoader();
                ShowNotification('success');
               // HideEditor();

            },
            error: function() {
                alert("error");
               // HideLoader();
            }
        });
    });

    $('#ResetFileContent').click(function(e) {

        if (currentFile != '' || currentFile != undefined) {
            ShowLoader();
            LoadFile(currentFile.name, currentFile.path);
        }

        e.stopPropagation();
    });

    $('#CancelFileContent').click(function() {
        HideEditor();
    });




    $('#directoryContainer').delegate('.folderItem', 'click', DirectoryItemClick);

    function DirectoryItemClick(e) {

        var $this = $(this);

        if ($this.attr('expanded') == '1') {

            $this.find('ul:first').slideDown('slow');
            $this.attr('expanded', '0');
            $this.addClass('folderItemOpened');

            if ($this.attr('isOpen') != '1') {

                ShowLoader();

                $.ajax({
                    type: "get",
                    url: "/Home/DirectoryDetails",
                    data: { directory: $this.attr('folderName') },
                    async: false,
                    success: function(data) {
                        AddDirectoryContent($this, data);
                        $this.find('ul:first').slideDown('slow');
                        $this.attr('isOpen', '1');
                        HideLoader();
                    },
                    error: function() {
                        alert("error");
                        HideLoader();
                    }
                });
            }
        }
        else {
            $this.find('ul:first').slideUp('slow');
            $this.attr('expanded', '1');
            $this.removeClass('folderItemOpened');
        }

        e.stopPropagation();
    }

    function DisplayDirectoryList(data) {
        $('#directoryContainer').html('');
        AddDirectoryContent($('#directoryContainer'), data);
        $('#directoryContainer').find('ul').show();
    }

    function AddDirectoryContent(element, data) {

        element.append('<ul style="display:none;">');

        $(data).each(function(idx, el) {

            if (el.ItemType == 1) {
                element.find('ul').append('<li fileName="' + el.Name + '" title="Date change ' + el.DateTime + '" class="fileItem"><span class="folderLink">' + el.Name + '</span></li>');
            }
            else {

                var folderName;

                if (element.is('li')) {
                    folderName = element.attr('folderName') + '/' + el.Name;
                }
                else {
                    folderName = el.Name;
                }

                element.find('ul').append('<li expanded="1" folderName="' + folderName + '" title="Дата изменения ' + el.DateTime + '" class="folderItem"><span class="folderLink">' + el.Name + '</span></li>');
            }

        });
    }

    $('#collapseAll').click(function() {
        $('#directoryContainer ul li').removeClass('folderItemOpened').attr('expanded', '1'); ;
        $('#directoryContainer ul:first li').find('ul').hide();
    });

});

function ShowLoader() {
    $('#loader').show();
}

function HideLoader() {
    $('#loader').hide();
}

function ShowEditor() {
    $('#fileInfoContent').show();
    $('#noFileContent').hide();
    $('#fileContentContainer').show();
}

function HideEditor() {
    $('#fileInfoContent').hide();

    var i = Math.floor(Math.random() * 3); //0-2

    var src = '../../Images/Characters/';

    switch (i) {
        case 0:
            src += 'bob.png';
            break;
        case 1:
            src += 'superman.png';
            break;
        case 2:
            src += 'pantera.png';
            break;
    }

    $('#noFileContent').find('img').attr('src', src);

    $('#noFileContent').show();
    $('#fileContentContainer').hide();


}

function ShowNotification(status) {

    var $container = $('#notificationSuccessContainer');

    if (status == 'error') {
        $container = $('#notificationErrorContainer');
    }

    $container.show().delay('3000').fadeOut();
}