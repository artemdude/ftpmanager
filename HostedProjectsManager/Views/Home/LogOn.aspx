﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Account.Master" Inherits="System.Web.Mvc.ViewPage<Credentials>" %>
<%@ Import Namespace="GetFilesContent.Controllers"%>

<asp:Content ID="PageTitleContent" ContentPlaceHolderID="PageTitleContent" runat="server">
    FACE CONTROL
</asp:Content>

<asp:Content ID="TitleContent" ContentPlaceHolderID="TitleContent" runat="server">FACE CONTROL</asp:Content>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
    <% using (Html.BeginForm())
       { %>
    <div class="title">
        Ftp address
    </div>
    <div class="editor-field">
        <%= Html.TextBoxFor(m => m.Domain, new{@class = "textbox"}) %>
         <div style=" font-size:9px; color:#ccc;">For example mysite.com or 193.19.222.8</div>
    </div>
    <div class="space20">
    </div>
    
    <table width="100%">
    <tr>
    <td>
     <div class="title">
        Login
    </div>
            <div class="">
        <%= Html.TextBoxFor(m => m.Login, new { @class = "textbox textboxSmall" })%>
    </div>
    
    </td> 
    <td width="20px"></td>
    <td>
    <div class="title">
       Password
    </div>
    <div class="">
        <%= Html.PasswordFor(m => m.Password, new { @class = "textbox textboxSmall" })%>
    </div>
    </td>
    </tr>
    </table>


    <div class="space20">
    </div>
    <table width="100%">
        <tr>
            <td>

            </td>
            <td align="right">
                <input type="submit" class="button" value="Enter" />
            </td>
        </tr>
    </table>
    <% } %>
</asp:Content>
