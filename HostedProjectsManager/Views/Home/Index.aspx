﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Hosted projects manager</title>

    <script src="<%= ResolveUrl("~/Scripts/jquery.js") %>" type="text/javascript"></script>

    <link href="<%= ResolveUrl("~/Css/style.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%= ResolveUrl("~/Css/spaces.css") %>" rel="stylesheet" type="text/css" />

    <script src="<%= ResolveUrl("~//Scripts/codepress/codepress.js") %>" type="text/javascript"></script>

    <script src="<%= ResolveUrl("~/Scripts/jquery.watermark.min.js") %>" type="text/javascript"></script>

    <script src="<%= ResolveUrl("~//Scripts/site.js") %>" type="text/javascript"></script>

</head>
<body>
    <div id="loginContainer">
        <div id="loginFieldsContainer">
            <input id="host" class="textbox" style="width: 245px" type="text" />
            <div class="space10">
            </div>
            <input id="login" class="textbox" style="width: 115px; margin-right: 10px;" type="text" />
            <input id="password" class="textbox" style="width: 109px" type="text" />
            <div class="space10">
            </div>
            <input id="ChangeFtp" class="button" type="button" value="Change" />
            <span id="changeFtpSuccess" style="display: none; color: Green;">Success</span>
            <span id="changeFtpError" style="display: none; color: Red;">Error</span>
        </div>
    </div>
    <div id="header">
        <div style="position: relative;">
       <div id="logo">Hosted Projects Manager</div>
            <div id="credentialInfo">
               <span id="hostName"><%= ViewData["Domain"] %></span> 
                | <a href="<%= Url.Action("Exit") %>">Exit</a>
            </div>
        </div>
    </div>
    <div id="mainContainer">
        <div id="leftPanel">
            <div id="leftMenuNavigation">
                <div style="position: relative">
                    <div id="leftMenuNavigationContent">
                        <span style="margin-right: 7px;" id="refresh" class="button">
                            <img alt="Refresh" style="vertical-align: middle" src="<%= ResolveUrl("~/Images/Site/refresh.png") %>" />
                            Refresh </span><span id="collapseAll" class="button">
                                <img alt="Collapse all" style="vertical-align: middle" src="<%= ResolveUrl("~/Images/Site/expand.png") %>" />
                                Collapse all</span>
                    </div>
                </div>
            </div>
            <div id="directoryContainer">
                <div style="position: relative; height: 100%;">
                    <div style="overflow: hidden; padding: 10px 0 0 10px;">
                        <div id="directoryContainer">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="rightPanel">
            <div id="notificationSuccessContainer" class="notificationContainer">
                <div class="notificationContent">
                    File was cuccessfuly saved
                </div>
            </div>
            <div id="notificationErrorContainer" class="notificationContainer">
                <div class="notificationContent">
                    Some error has been occured
                </div>
            </div>
            <div id="fileInfoContainer">
                <div id="fileInfoContentWrapper">
                    <div id="toggleDirectoryPanel" status="visible">
                        <img src="<%= ResolveUrl("~/Images/Site/hide.png") %>" />
                    </div>
                    <div id="fileInfoContent" style="display: none;">
                        <span id="filePath"></span>
                        <div>
                        </div>
                        <span id="fileName"></span>
                        <div style="position: absolute; top: 25px; right: 20px;">
                            <span id="SaveFileContent" style="margin-right: 7px;" class="button">
                                <img style="vertical-align: middle" src="<%= ResolveUrl("~/Images/Site/ok_green.png") %>" />
                                Save</span><span id="ResetFileContent" class="button" style="margin-right: 7px;"><img
                                    style="vertical-align: middle" src="<%= ResolveUrl("~/Images/Site/reset.png") %>" />
                                    Reset</span><span id="CancelFileContent" class="button"><img alt="Cancel" style="vertical-align: middle"
                                        src="<%= ResolveUrl("~/Images/Site/cancel.gif") %>" />
                                        Cancel</span>
                        </div>
                    </div>
                </div>
            </div>
            <div id="fileContentWrapper">
                <div style="position: relative; height: 100%;">
                    <div style="overflow: auto; padding: 20px;">
                        <div id="noFileContent">
                            Select file for editing...
                            <div style="height: 30px;">
                            </div>
                            <img src="<%= ResolveUrl("~/Images/Characters/bob.png") %>" />
                            <div id="me">
                                Created by Atem Shkolovyi
                            </div>
                        </div>
                        <div id="fileContentContainer">
                            <textarea id="fileContentEditor" class="codepress css"></textarea>
                            <textarea currentfile="" id="fileContent"></textarea>
                        </div>
                    </div>
   
                </div>
            </div>
        </div>
    </div>
    <div id="loader">
        Loading...
    </div>
 
</body>
</html>
