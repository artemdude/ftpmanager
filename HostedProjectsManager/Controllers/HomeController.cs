﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace GetFilesContent.Controllers
{
    public class HomeController : Controller
    {
        public Credentials Credentials
        {
            get
            {
                if (Request.Browser.Cookies)
                {
                    var js = new JavaScriptSerializer();

                    if (Request.Cookies["Credentials"] != null)
                    {
                        return js.Deserialize<Credentials>(Request.Cookies["Credentials"].Value);
                    }
                  
                        return null;
                 
                }

                return (Credentials) Session["Credentials"];
            }

            set
            {
                if (Request.Browser.Cookies)
                {
                    var credentials = new HttpCookie("Credentials");

                    var js = new JavaScriptSerializer();

                    credentials.Value = js.Serialize(value);

                    Response.Cookies.Add(credentials);
                }
                else
                {
                    Session["Credentials"] = value;
                }
            }
        }

        private static bool TyConnectFtp(string domain, string login, string password)
        {
            try
            {
                var request = (FtpWebRequest) WebRequest.Create("ftp://" + domain);
                request.Credentials = new NetworkCredential(login, password);
                request.KeepAlive = false;
                request.Method = WebRequestMethods.Ftp.ListDirectoryDetails;

                request.GetResponse();

                return true;
            }
            catch
            {
                return false;
            }
        }

        public ActionResult LogOn()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LogOn(Credentials credentials)
        {
            if (TyConnectFtp(credentials.Domain, credentials.Login, credentials.Password))
            {
                Credentials = credentials;

                return RedirectToAction("Index");
            }
            return View();
        }

        public ActionResult Index()
        {
            if (Credentials == null)
            {
                return RedirectToAction("LogOn");
            }

            ViewData["Domain"] = Credentials.Domain;

            return View();
        }

        public ActionResult Exit()
        {
            Credentials = null;

            return RedirectToAction("LogOn");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ReadFile(string file, string directory)
        {
            WebResponse response =
                GetFtpWebRequest(directory, file, WebRequestMethods.Ftp.DownloadFile).GetResponse();

            Stream responseStream = response.GetResponseStream();

            var reader = new StreamReader(responseStream, Encoding.GetEncoding(1251));

            string fileContent = reader.ReadToEnd();

            var fileObj = new FileContent
                              {
                                  Name = file,
                                  Content = fileContent
                              };

            reader.Close();
            response.Close();

            return Json(fileObj, JsonRequestBehavior.AllowGet);
        }


        [AcceptVerbs(HttpVerbs.Post), ValidateInput(false)]
        public ActionResult SaveFile(string file, string directory, string content)
        {
            try
            {
                FtpWebRequest response = GetFtpWebRequest(directory, file, WebRequestMethods.Ftp.UploadFile);

                Stream requestStream = response.GetRequestStream();

                byte[] buffer = Encoding.Default.GetBytes(content);

                requestStream.Write(buffer, 0, buffer.Length);

                requestStream.Close();

                return Json(new ProcessStatus {IsError = false}, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new ProcessStatus {IsError = true}, JsonRequestBehavior.AllowGet);
            }
        }


        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult DirectoryDetails(string directory)
        {
            //todo:to filter
            if (Credentials == null)
            {
                return RedirectToAction("LogOn");
            }

            WebResponse response =
                GetFtpWebRequest(directory, null, WebRequestMethods.Ftp.ListDirectoryDetails).GetResponse();

            Stream responseStream = response.GetResponseStream();

            var reader = new StreamReader(responseStream);

            string folderDetails = reader.ReadToEnd();

            string[] folderDetailsArray = folderDetails.Split(new[] {"\r\n"}, StringSplitOptions.None);


            var directories = new List<FtpDirectoryDetails>();

            foreach (string item in folderDetailsArray)
            {
                const string pattern = @"((\d{2}-\d{2}-\d{2}) *(\d{2}:\d{2}(PM|AM))) *(\d+|<DIR>) *(.*)";

                MatchCollection matches = Regex.Matches(item, pattern, RegexOptions.Singleline | RegexOptions.Multiline);

                foreach (Match match in matches)
                {
                    directories.Add(new FtpDirectoryDetails
                                        {
                                            Name = match.Groups[6].Value,
                                            DateTime = match.Groups[1].Value,
                                            ItemType =
                                                match.Groups[5].Value == "<DIR>"
                                                    ? FtpDirectoryDetails.Type.Directory
                                                    : FtpDirectoryDetails.Type.File
                                        });
                }
            }

            reader.Close();
            response.Close();

            return Json(directories, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ChangeFtp(string domain, string login, string password)
        {
            if (TyConnectFtp(domain, login, password))
            {
                Credentials = new Credentials {Domain = domain, Login = login, Password = password};

                return Json(new ProcessStatus {IsError = false}, JsonRequestBehavior.AllowGet);
            }

            return Json(new ProcessStatus {IsError = true}, JsonRequestBehavior.AllowGet);
        }


        private FtpWebRequest GetFtpWebRequest(string folder, string file, string method)
        {
            var request = (FtpWebRequest) WebRequest.Create("ftp://" + Credentials.Domain + "/" + folder + "/" + file);
            request.Credentials = new NetworkCredential(Credentials.Login, Credentials.Password);
            request.KeepAlive = false;
            request.Method = method;
            request.UseBinary = true;
            request.Proxy = null;

            return request;
        }
    }

    [Serializable]
    public class Credentials
    {
        public string Domain { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    }

    public class ProcessStatus
    {
        public bool IsError { get; set; }
    }

    public class FileContent
    {
        public string Name { get; set; }
        public string Content { get; set; }
    }

    public class FtpDirectoryDetails
    {
        #region Type enum

        public enum Type
        {
            Directory,
            File
        }

        #endregion

        public string DateTime { get; set; }
        public string Name { get; set; }
        public Type ItemType { get; set; }
    }
}